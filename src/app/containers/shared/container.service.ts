import { Injectable } from '@angular/core';
import { ApiService } from '../../core';

@Injectable()
export class ContainerService {
  path: String = '/containers';

  constructor(private apiService: ApiService) {
  }

  getContainers() {
    return this.apiService.get(this.path);
  }
}
