import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {
  ContainersComponent,
  ContainerListComponent,
  ContainerRowComponent,
  ContainerNamePipe,
  ContainerService
} from './containers/';

import { ApiService } from './core';
import { DashboardComponent } from './dashboard';
import { routes } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    ContainersComponent,
    ContainerListComponent,
    ContainerRowComponent,
    ContainerNamePipe,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    ApiService,
    ContainerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
